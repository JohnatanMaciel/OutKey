import React from 'react';
import Fetch from 'react-fetch';

// Components
import NavBar               from './components/navbar/navbar.component';
import OpeningHome          from './components/openingHome/openingHome.component';
import Apresentation        from './components/apresentation/apresentation.component';
import Available            from './components/available/available.component';
import YourCompany          from './components/yourCompany/yourCompany.component';
import WhatService          from './components/whatService/whatService.component';
import SlideTestimonials    from './components/slideTestimonials/slideTestimonials.component';
import Footer               from './components/footer/footer.component';

export default React.createClass({

  render() {
    return (
      <Fetch url="http://www.jumper.digital/outkey/wp-json/wp/v2/pages/2/" onError={this.onError}>
        <NavBar />
        <OpeningHome />
        <Apresentation />
        <Available />
        <YourCompany />
        <WhatService />
        <SlideTestimonials />
        <Footer />
      </Fetch>
    )
  }

})