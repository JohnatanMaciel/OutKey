import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';

// Pages
import App        from './App';
import About      from './About';
import HowItWorks from './HowItWorks';
import Faq        from './Faq';
import Contact    from './Contact';

require('./pattern.scss');

ReactDOM.render((

  <Router history={hashHistory}>
    <Route path="/" component={App}></Route>
    <Route path="/sobre" component={About}></Route>
    <Route path="/como-funciona" component={HowItWorks}></Route>
    <Route path="/perguntas-frequentes" component={Faq}></Route>
    <Route path="/contato" component={Contact}></Route>
  </Router>

), document.getElementById('OutKey'));