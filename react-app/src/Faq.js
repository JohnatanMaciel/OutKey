import React from 'react';
import Fetch from 'react-fetch';

// Components
import NavBar       from './components/navbar/navbar.component';
import Footer       from './components/footer/footer.component';
import WhatService  from './components/whatService/whatService.component';
import ClientBrands from './components/clientBrands/clientBrands.component';

// Page component
import Answers from './pages/faq/answers.component';

export default React.createClass({

  render() {
    return (
      <div className="page faq-page header-alt page-title_section">
        <NavBar />
          <Fetch url="http://www.jumper.digital/outkey/wp-json/wp/v2/pages/14/" onError={this.onError}>
            <Answers />
            <WhatService />
            <ClientBrands />
          </Fetch>
        <Footer />
      </div>
    )
  }

})