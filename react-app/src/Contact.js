import React from 'react';
import Fetch from 'react-fetch';

// Components
import NavBar from './components/navbar/navbar.component';
import Footer from './components/footer/footer.component';

// Page component
import Contact from './pages/contact/contact.component';

export default React.createClass({

  render() {
     return (
        <div className="page contact-page header-alt page-title_section">
          <NavBar />
            <Fetch url="http://www.jumper.digital/outkey/wp-json/wp/v2/pages/46/" onError={this.onError}>
               <Contact />
            </Fetch>
          <Footer />
        </div>
     )
  }

})