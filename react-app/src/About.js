import React from 'react';
import Fetch from 'react-fetch';

// Components
import NavBar  from './components/navbar/navbar.component';
import Footer  from './components/footer/footer.component';

// Page component
import OpeningAbout  from './pages/about/opening.component';

export default React.createClass({

  render() {
     return (
        <div className="page about-page header-alt">
          <NavBar />
            <Fetch url="http://www.jumper.digital/outkey/wp-json/wp/v2/pages/10/" onError={this.onError}>
               <OpeningAbout />
            </Fetch>
          <Footer />
        </div>
     )
  }

})