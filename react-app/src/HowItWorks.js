import React from 'react';
import Fetch from 'react-fetch';

// Components
import NavBar       from './components/navbar/navbar.component';
import WhatService  from './components/whatService/whatService.component';
import ClientBrands from './components/clientBrands/clientBrands.component';
import Footer       from './components/footer/footer.component';

// Page component
import OpeningItWorks from './pages/howitworks/openingItWorks.component';

export default React.createClass({

  render() {
    return (
      <div className="page howitworks-page header-alt">
        <NavBar />
        <Fetch url="http://www.jumper.digital/outkey/wp-json/wp/v2/pages/12/" onError={this.onError}>
          <OpeningItWorks />
          <WhatService />
          <ClientBrands />
        </Fetch>
        <Footer />
      </div>
    )
  }

})