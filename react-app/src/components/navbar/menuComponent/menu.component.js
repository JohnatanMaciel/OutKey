import React, { Component } from 'react';
import { Link }             from 'react-router';

class MenuLinks extends Component {

    toTop() {
      document.body.scrollTop = 0
    }

   render() {
      return (
          <nav>

             <ul className="navbarList">
                <li><Link to="/" activeClassName="active" onClick={this.toTop}><span>Inicio</span></Link></li>
                <li><Link to="/sobre" activeClassName="active" onClick={this.toTop}><span>Sobre</span></Link></li>
                <li><Link to="/como-funciona" activeClassName="active" onClick={this.toTop}><span>Como Funciona</span></Link></li>
                <li><Link to="/perguntas-frequentes" activeClassName="active" onClick={this.toTop}><span>FAQ</span></Link></li>
                <li><Link to="/contato" activeClassName="active" onClick={this.toTop}><span>Contato</span></Link></li>

                <li className="social-media_head">
                  <ul>
                    <li><a href="https://www.facebook.com/OutKey/"><span className="fa fa-facebook"></span></a></li>
                    <li><a href="https://www.instagram.com/outkeyapp/"><span className="fa fa-instagram"></span></a></li>
                    <li><a href="#"><span className="fa fa-twitter"></span></a></li>
                    <li><a href="https://www.youtube.com/watch?v=Q_llm6HtzYM&t=9s"><span className="fa fa-youtube"></span></a></li>
                  </ul>
                </li>
             </ul>

             <ul className="navbarList downloads">
                <li>
                  <a target="_blank" href="https://play.google.com/store/apps/details?id=br.com.deway.outkey&hl=pt_BR">
                      <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/google-play-new.png" role="presentation"/>
                  </a>
                </li>
                <li>
                  <a target="_blank" href="https://itunes.apple.com/br/app/outkey/id1038170460?mt=8">
                      <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/app-store-icon.png" role="presentation"/>
                  </a>
                </li>
             </ul>

          </nav>
      )
   }

}

export default MenuLinks;