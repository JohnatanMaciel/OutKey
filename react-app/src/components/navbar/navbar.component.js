import React, { Component } from 'react';
import { Link }             from 'react-router';
import MenuLinks            from './menuComponent/menu.component';

require('./navbar.style.scss');

class NavBar extends Component {

    constructor(props) {
      super(props);

      this.state = { toggle: false };
      this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {
        this.setState(prevState => ({
            toggle: !prevState.toggle
        }));
    }

    render() {
        return (
            <header className={this.state.toggle ? 'navbar-open' : ''}>
                <div className="content">
                   <div className="flex-header">
                      <h1>
                        <Link to="/">
                            <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/logotipo.png" role="presentation" />
                        </Link>
                      </h1>
                      <MenuLinks />
                      <button className="menu-open" onClick={this.handleClick}>
                         <span></span>
                      </button>
                   </div>
                </div>
            </header>
        )
    }

}

export default NavBar;