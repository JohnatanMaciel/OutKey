import React, { Component }  from 'react';
import Modal, { closeStyle } from 'simple-react-modal';

require('./openingHome.style.scss');

class OpeningHome extends Component {

    constructor(){
        super()

        this.animateHome();

        this.state = {
            homeTyped: [],
        };
    }

    componentDidMount() {

        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/2/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const homeTyped = res.acf.typed_itens.map(obj => obj);
            this.setState({ homeTyped });

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    show(){
        this.setState({show: true})
    }

    close(){
        this.setState({show: false})
    }

    animateHome() {
        setTimeout(function(){
            document.getElementById("typing-4").style.display = 'block';
        }, 19600);
    }

    render() {

        let GIFload = 'http://jumper.digital/outkey/wp-content/uploads/2016/12/loading.gif';

        return(
            <div className="wrap-content_bg" style={{backgroundImage: `url(${this.props.acf ? this.props.acf['bg_image'] : 'Carregando ...'})`}}>
                <div className="content">

                    <div className="primary-title">
                        <h3 id="wrap-typing">
                            Se identificar nunca mais vai ser
                            <span className="wrap-typing">
                                
                                {this.state.homeTyped.map((post, i) =>
                                    <span key={i}>{post.text_typed}</span>
                                )}
                                <span id="typing-4">{ this.props.acf ? this.props.acf['text_typed_last'] : ''}</span>

                            </span>
                        </h3>
                        <p>
                            { this.props.acf ? this.props.acf['description-title'] : 'Carregando ...'}
                        </p>
                    </div>

                    <div className="flex-opening">
                        <div className="outkey-movie">
                            <a href="#" onClick={this.show.bind(this)}>
                                <span></span>
                                <p>Assista <b>Nosso vídeo</b></p>
                            </a>
                        </div>
                        <div className="mobile-device_app wow animate-fade" data-wow-duration="2s" data-wow-delay="0s">
                            <img src={this.props.acf ? this.props.acf['app-image'] : GIFload} alt="OutKey App" />
                        </div>
                        <div className="app-download_stories">
                            <a className="play-story" href="https://play.google.com/store/apps/details?id=br.com.deway.outkey&hl=pt_BR" target="_blank">
                                <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/google-play-new.png" role="presentation"/>
                            </a>
                            <a className="apple-story" href="https://itunes.apple.com/br/app/outkey/id1038170460?mt=8" target="_blank">
                                <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/app-store-icon.png" role="presentation"/>
                            </a>
                        </div>
                    </div>

                    <Modal className="modal-component" closeOnOuterClick={true} show={this.state.show} onClick={this.close.bind(this)} containerClassName="modal-wrap">
                        <a style={closeStyle} onClick={this.close.bind(this)}><span className="fa fa-close"></span></a>
                        <div className="modal-iframe">
                            <iframe src="https://www.youtube.com/embed/Q_llm6HtzYM?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </Modal>

                </div>
            </div>
        )
    }

}


export default OpeningHome;

