import React, { Component } from 'react';
require('./yourCompany.style.scss');

class YourCompany extends Component {

    constructor(props) {
      super(props);

      this.state = {
        yourCompany: [],
      };
    }

    componentDidMount() {

        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/2/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const yourCompany = res.acf.company_featured_repeater.map(obj => obj);
            this.setState({ yourCompany });

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    render() {

        return (
            <section className="home-your-company">
                <div className="content flex-your-company">

                    <div className="section-title">
                        <h2>Para o seu estabelecimento.</h2>
                    </div>

                    <div className="wrap-articles-goals">

                        {this.state.yourCompany.map((post, i) =>
                            <article className="wow animate-fade" data-wow-duration="0.5s" key={i}>
                                <div className="wrap-title">
                                    <span className={post.company_featured_icon}></span>
                                    <h3>{post.company_featured_title}</h3>
                                </div>
                                <p>
                                    {post.company_featured_description}
                                </p>
                            </article>
                        )}

                    </div>

                </div>
            </section>
        )
    }

}

export default YourCompany;