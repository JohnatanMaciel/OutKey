import React, {Component} from 'react';

class Accordion extends Component{

    constructor(props) {
      super(props);

      this.state = {
        toggleAcordion: false
      };

      this.handleAccordion = this.handleAccordion.bind(this);

    }

    handleAccordion() {
        this.setState(prevState => ({
            toggleAcordion: !prevState.toggleAcordion
        }));
    }

    render(){
        return (
            <li className="wow animate-fade" data-wow-duration="1s">
                <button className="question" onClick={this.handleAccordion}>
                    <span className="question-icon">
                        <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/icon-question.png" alt="OutKey"/>
                    </span>
                    <p>{this.props.title}</p>
                </button>
                <div className={this.state.toggleAcordion ? 'open-answers answers' : 'answers'}>
                    <p>
                        {this.props.children}
                    </p>
                </div>
            </li>
        )
    };

}

export default Accordion;
