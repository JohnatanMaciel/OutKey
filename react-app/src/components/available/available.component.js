import React, { Component } from 'react';
require('./available.style.scss');

class Available extends Component {

    render() {
        return (
            <section className="home-available" style={{backgroundImage: `url(${this.props.acf ? this.props.acf['background_image_downloads'] : 'Carregando ...'})`}}>
                <span className="top-circle"></span>
                <div className="content">
                    <div className="mobile-devices_illustration">
                        <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/app-outkey-home-mobiles.png" role="presentation"/>
                    </div>
                    <article className="download-available wow animate-fade_right" data-wow-duration="0.7s">
                        <h2>{this.props.acf ? this.props.acf['title_section_dowloads'] : 'Carregando ...'}</h2>
                        <p>
                            {this.props.acf ? this.props.acf['description_section_downloads'] : 'Carregando ...'}
                        </p>
                        <ul>
                            <li><a href="https://itunes.apple.com/br/app/outkey/id1038170460?mt=8" target="_blank"><span className="fa fa-apple"></span></a></li>
                            <li><a href="https://play.google.com/store/apps/details?id=br.com.deway.outkey&hl=pt_BR" target="_blank"><span className="fa fa-android"></span></a></li>
                        </ul>
                    </article>
                </div>
            </section>
        )
    }

}

export default Available;