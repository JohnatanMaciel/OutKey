import React, { Component } from 'react';
import Carousel from 'nuka-carousel';
import ItemTestimonials from './itemTestimonials.component';

require('./slideTestimonials.style.scss');

class SlideTestimonials extends Component {

    constructor(props) {
      super(props);

      this.state = {
        testimonials: [],
      };
    }

    componentDidMount() {

        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/2/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const testimonials = res.acf.testimonials_repeater.map(obj => obj);
            this.setState({ testimonials });
            console.log(testimonials);

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    render() {

        return (
            <section className="carouselTestimonials">

                <div className="icon-testimonials">
                    <span className="fa fa-quote-left"></span>
                </div>

                 <Carousel slidesToShow={1} slidesToScroll={1} speed={700}>

                    {this.state.testimonials.map((item, index) =>
                        <ItemTestimonials key={index}
                            testimonial={item.testimonials_text} picture={item.client_picture}
                            clientname={item.client_name} company={item.company_and_job}/>
                    )}

                 </Carousel>

            </section>
        )
    }

}

export default SlideTestimonials;