import React, { Component } from 'react';

class ItemTestimonials extends Component {

    render() {
        return (

            <div className="wrap-testimonial">
                <div className="content-testimonial">
                    <p>
                        {this.props.testimonial}
                    </p>
                    <div className="thumb-author">
                        <span className="radius-thumb">
                            <span>
                                <img src={this.props.picture} role="presentation"/>
                            </span>
                        </span>
                        <div className="description-author">
                            <h2>{this.props.clientname}</h2>
                            <p>{this.props.company}</p>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default ItemTestimonials;