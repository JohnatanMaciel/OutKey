import React, { Component } from 'react';
import { Link }             from 'react-router';
require('./whatService.style.scss');

class WhatService extends Component {

    toTop() {
      document.body.scrollTop = 0
    }

    render() {

        let imgURL  = "http://jumper.digital/outkey/wp-content/uploads/2016/12/city-wallpaper-47.jpg";
        const bgIMG = {
            backgroundImage: `url(${imgURL})`,
        };

        return (
            <section className="what-a-services" style={bgIMG}>
                <div className="content">
                    <div className="title-section">
                        <h2>Oque Atendemos</h2>
                    </div>
                    <div className="flex-content_services">
                        <article className="wow animate-fade" data-wow-duration="0.5s">
                            <span className="fa fa-home"></span>
                            <p>Condomínios</p>
                        </article>
                        <article className="wow animate-fade" data-wow-duration="0.5s">
                            <span className="fa fa-globe"></span>
                            <p>Empresas e organizações</p>
                        </article>
                        <article className="wow animate-fade" data-wow-duration="0.5s">
                            <span className="fa fa-graduation-cap"></span>
                            <p>Universsidade e escolas</p>
                        </article>
                        <article className="wow animate-fade" data-wow-duration="0.5s">
                            <span className="fa fa-building"></span>
                            <p>Hoteis</p>
                        </article>
                        <article className="wow animate-fade" data-wow-duration="0.5s">
                            <span className="fa fa-user-md"></span>
                            <p>Hospitais e clínicas</p>
                        </article>
                        <article className="wow animate-fade" data-wow-duration="0.5s">
                            <span className="fa fa-glass"></span>
                            <p>Espaço para eventos</p>
                        </article>
                        <article className="wow animate-fade" data-wow-duration="0.5s">
                            <span className="fa fa-coffee"></span>
                            <p>Escritórios e coworkings</p>
                        </article>
                        <Link onClick={this.toTop} to="/contato" className="alt-bloc wow animate-fade" data-wow-duration="0.5s">
                            <span></span>
                            <p>Sua empresa <b>Entre em contato</b></p>
                        </Link>
                    </div>
                </div>
            </section>
        )
    }

}

export default WhatService;