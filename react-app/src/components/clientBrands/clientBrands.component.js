import React, { Component } from 'react';
import Carousel from 'nuka-carousel';
require('./clientBrands.style.scss');

class ClientBrands extends Component {

    constructor(props) {
      super(props);

      this.state = {
        clients: []
      };
    }

    componentDidMount() {

        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/10/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const clients = res.acf.outkey_clients.map(obj => obj.clients_size);
            this.setState({ clients });

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    render() {

        return (
            <section className="list-clients">
                <div className="content">
                    <div className="wgt flex-clients">

                        <Carousel className="desktop" slidesToShow={5} slidesToScroll={2} speed={700} cellSpacing={40}>
                          {this.state.clients.map((item, index) =>
                              <img key={index} src={item.url} alt="OutKey App"/>
                          )}
                        </Carousel>

                        <Carousel className="mobile" slidesToShow={2} slidesToScroll={2} speed={700} cellSpacing={40}>
                          {this.state.clients.map((item, index) =>
                              <img key={index} src={item.url} alt="OutKey App"/>
                          )}
                        </Carousel>

                    </div>
                </div>
            </section>
        )
    }

}

export default ClientBrands;