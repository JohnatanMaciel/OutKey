import React, { Component } from 'react';
import { Link }             from 'react-router';
import MenuLinks            from './../navbar/menuComponent/menu.component';

require('./footer.style.scss');

class Footer extends Component {

   toTop() {
      document.body.scrollTop = 0
   }

   render() {
      let imgURL  = "http://jumper.digital/outkey/wp-content/uploads/2016/12/city-wallpaper-47.jpg";
      const bgIMG = {
         backgroundImage: `url(${imgURL})`,
      };

      return (
         <footer className="footer-component" style={bgIMG}>
            <div className="content">
               <div className="social-media">
                  <h2>Acompanhe-nos</h2>
                  <ul>
                     <li><a target="_blank" href="https://www.facebook.com/OutKey/" className="facebook-icon"><span className="fa fa-facebook"></span></a></li>
                     <li><a target="_blank" href="https://www.instagram.com/outkeyapp/" className="instagram-icon"><span className="fa fa-instagram"></span></a></li>
                     <li><a target="_blank" href="#" className="twitter-icon"><span className="fa fa-twitter"></span></a></li>
                     <li><a target="_blank" href="https://www.youtube.com/watch?v=Q_llm6HtzYM&t=9s" className="youtube-icon"><span className="fa fa-youtube"></span></a></li>
                  </ul>
               </div>
               <div className="flex-footer">
                  <MenuLinks />
                  <div className="brand-jumper-digital">
                     <a target="_blank" href="http://jumper.digital">
                        <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/profile-jumper.png" alt="Jumper Digital"/>
                     </a>
                  </div>
               </div>
               <div className="brand-outkey">
                  <Link to="/" onClick={this.toTop}>
                     <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/logotipo.png" alt="Outkey"/>
                  </Link>
               </div>
            </div>
         </footer>
      )
   }

}

export default Footer;