import React, { Component } from 'react';
require('./apresentation.style.scss');

class Apresentation extends Component {

    constructor(props) {
      super(props);

      this.state = {
        apresentation: [],
      };
    }

    componentDidMount() {

        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/2/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const apresentation = res.acf.qualities_repeater.map(obj => obj);
            this.setState({ apresentation });

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    render() {

        let imgURL = "http://jumper.digital/outkey/wp-content/uploads/2016/12/mobile-phone-app-section.png";
        const styleImagePhone = {
            backgroundImage: `url(${imgURL})`,
        };

        return (
            <section className="home-apresentation">

                <article className="primary-apresentation">
                    <div className="apresentation-content">
                        <span className="wow animate-fade" data-wow-duration="0.5s">
                            <img src="http://jumper.digital/outkey/wp-content/uploads/2016/12/mobile-icon.png" role="presentation"/>
                        </span>
                        <p className="wow animate-fade" data-wow-duration="1s">
                            { this.props.acf ? this.props.acf['presentation_text'] : 'Carregando ...' }
                        </p>
                    </div>
                </article>

                <article>
                    <div className="content">
                        <div className="master-objective" style={styleImagePhone}>
                            <ul>

                                {this.state.apresentation.map((post, i) =>
                                    <li className="wow animate-fade_left" data-wow-duration="0.7s" key={i}>
                                        <div className="article_objective">
                                            <div className="the-content_objective">
                                                <h2>{post.title_quality}</h2>
                                                <p>
                                                    {post.quality_description}
                                                </p>
                                            </div>
                                            <div className="icon-article">
                                                <span className={post.icon_quality}></span>
                                            </div>
                                        </div>
                                    </li>
                                )}

                            </ul>
                        </div>
                    </div>
                </article>

            </section>
        )
    }
}

export default Apresentation;