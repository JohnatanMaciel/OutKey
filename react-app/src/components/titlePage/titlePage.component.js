import React, { Component } from 'react';
require('./titlePage.style.scss');

class TitlePage extends Component{

  render() {
     return (
        <section className="title-opening">
            <h2>{this.props.pageTitle}</h2>
        </section>
     )
  }

}

export default TitlePage;
