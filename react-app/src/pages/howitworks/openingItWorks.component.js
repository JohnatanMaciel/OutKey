import React, { Component } from 'react';
import { Link }             from 'react-router';
require('./openingItWorks.style.scss');

class OpeningItWorks extends Component{

    constructor(props) {
      super(props);

      this.state = {
        howItWorks: [],
      };
    }

    toTop() {
        document.body.scrollTop = 0
    }

    componentDidMount() {
        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/12/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const howItWorks = res.acf.how_it_works.map(obj => obj);
            this.setState({ howItWorks });

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    render() {

     let GIFload = "http://www.jumper.digital/outkey/wp-content/uploads/2016/11/loading.gif";

     return (
        <div className="openingitworks-content">

            <section className="wrap-opening">
                <div className="content">

                    <div className="flex-opening">
                        <div className="col-left">
                            <img src={ this.props.acf ? this.props.acf['work_image_opening'] : GIFload } alt="OutKey App"/>
                        </div>
                        <div className="col-right wow animate-fade" data-wow-duration="0.5s" data-wow-delay="0s">
                            <h2>{ this.props.acf ? this.props.acf['work_page_title'] : "Carregando ..." }</h2>
                            <p>{ this.props.acf ? this.props.acf['work_description_page'] : "Carregando ..." }</p>
                        </div>
                    </div>

                </div>
            </section>

            <section className="how-it-works">
                <div className="content">

                    <div className="title-section wow animate-fade" data-wow-duration="0.5s" data-wow-delay="0s">
                        <h2>Vamos agora ao passo a passo de como tudo isso funciona:</h2>
                    </div>

                    <div className="how-it-works_list">

                        {this.state.howItWorks.map((post, index) =>
                            <div className="how-it-works_item" key={index}>
                                <div className="icon-about wow animate-fade" data-wow-duration="1s">
                                    <img src={post.icon_instruction} alt="OutKey App"/>
                                </div>
                                <div className="the-content-work">
                                    <h2>
                                        {post.instruction_title}
                                    </h2>
                                    <p>{post.instruction_description}</p>
                                </div>
                            </div>
                        )}

                    </div>

                    <div className="anchor-faq">
                        <h1>
                            Dúvidas?
                        </h1>
                        <p>Consulte nossas dúvidas frequentes.</p>
                        <Link onClick={this.toTop} to="/perguntas-frequentes">Faq</Link>
                    </div>

                </div>
            </section>

        </div>
     )
    }

}

export default OpeningItWorks;
