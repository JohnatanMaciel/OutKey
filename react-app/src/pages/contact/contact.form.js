import React, { Component } from 'react';
import $ from 'jquery';
import MaskedInput from 'react-maskedinput';


class ContactForm extends Component{

  constructor(){
      super()
      this.state = {
          tel: [],
      };
  }

  handleSubmit(event) {

    let name    = $('#name').val(),
        email    = $('#email').val(),
        phone   = $('#phone').val(),
        city    = $('#city').val(),
        estate  = $('#estate').val(),
        message = $('#message').val();

    let urlData = `&name=${name}&email=${email}&phone=${phone}&city=${city}&estate=${estate}&message=${message}`;
    let contactForm = $("#contact-form");

    $.ajax({
         type: "POST",
         url: "http://jumper.digital/outkey/app/sendmail.php",
         async: true,
         data: urlData,

         success: function() {
             console.log("Enviando...")
         },

         beforeSend: function() {
            contactForm.find(".content-form").remove()
            contactForm.append(`
                <div class="sending-form">
                    <span class="loading-wrap">
                        <img src="http://serlares.com.br/wp-content/uploads/2016/11/loading.gif"/>
                    </span>
                </div>
            `);
         },

         complete: function(){
            console.log("Enviado!");

            contactForm.find(".content-form").remove();
            contactForm.find(".sending-form").remove();
            contactForm.append(`
                <div class="success-form">
                    <h1>Formulário enviado com sucesso!</h1>
                    <p>Agradecemos o contato, em breve retornaremos sua mensagem! =D</p>
                    <span class="fa fa-check-circle"></span>
                </div>
            `);
         }

     });

    event.preventDefault();

  }

  _onChange(e) {
    var stateChange = {}
    stateChange[e.target.name] = e.target.value
    this.setState(stateChange)
  }


  render() {
    return (
      <form onSubmit={this.handleSubmit} id="contact-form">
        <div className="content-form">

            <div className="row-form two">
                <div className="input-default">
                    <input id="name" type="text" required/>
                    <label>Seu nome</label>
                </div>
                <div className="input-default">
                    <MaskedInput id="phone" mask="(11) 11111 1111" placeholder="" name="tel" size="20" onChange={this._onChange} required/>
                    <label>Telefone</label>
                </div>
            </div>

            <div className="row-form">
                <div className="input-default">
                    <input id="email" type="text" required/>
                    <label>Seu E-mail</label>
                </div>
            </div>

            <div className="row-form two">
                <div className="input-default">
                    <input id="city" type="text" required/>
                    <label>Cidade</label>
                </div>
                <div className="input-default">
                    <input id="estate" type="text" required/>
                    <label>Estado</label>
                </div>
            </div>

            <div className="row-form">
                <div className="textarea-default">
                    <textarea id="message" placeholder="Menssagem"></textarea>
                </div>
            </div>

            <div className="env-form">
                <button type="submit" className="submit-form">Solicitar atendimento</button>
            </div>

        </div>
      </form>
    );
  }

}

export default ContactForm;
