import React, { Component } from 'react';

// Components
import TitlePage    from './../../components/titlePage/titlePage.component';
import ClientBrands from './../../components/clientBrands/clientBrands.component';
import ContactForm  from './contact.form';

// Styles
require('./contact.style.scss');

class Contact extends Component{

    render() {
        return (
            <section className="wrap-opening wrap-alt">

                <TitlePage pageTitle="Entre em contato" />

                <div className="content">
                    <div className="wrap-contact flex-cols">

                        <div className="col-left">

                            <p>Ficou interessado?</p>
                            <h2>Solicite um atendimento:</h2>

                            <ContactForm/>

                        </div>

                        <div className="col-right">

                            <div className="wgt wgt-about-us">
                                <div className="title-wgt">
                                    <h2>Sobre nós</h2>
                                </div>
                                <p>
                                    “Estamos em uma era em que a tecnologia
                                    está cada vez mais avançada, e nada mais
                                    gratificante que usufruir do melhor que ela
                                    nos oferece. Praticidade.”
                                </p>
                                <h2>- Nós da OutKey</h2>
                            </div>

                            <div className="wgt wgt-about-us">
                                <div className="title-wgt">
                                    <h2>Contato</h2>
                                </div>
                                <ul>
                                    <li>
                                        <a href="mailto:contato@outkey.com.br"><span className="fa fa-envelope"></span><p>contato@outkey.com.br</p></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="fa fa-phone"></span><p>85 3032.9737</p></a>
                                    </li>
                                    <li>
                                        <a href="#"><span className="fa fa-mobile-phone"></span><p>85 98181.1502</p></a>
                                    </li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>

                <ClientBrands />

            </section>
        )
    }

}

export default Contact;
