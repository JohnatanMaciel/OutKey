import React, { Component } from 'react';
import { Link }             from 'react-router';
import TitlePage            from './../../components/titlePage/titlePage.component';
import Accordion            from './../../components/accordion/accordion.component';
import { Tab, Tabs,
         TabList, TabPanel 
       }                    from 'react-tabs';

import $ from 'jquery';
require('./answers.style.scss');

class Answers extends Component{

    constructor(props) {
      super(props);

      this.state = {
        answersRepeater: [],
        answersRepeaterCompany: []
      }

    }

    toTop() {
        document.body.scrollTop = 0
    }

    submitQuestion(event) {

        let q_name      = $('#q_name').val(),
            q_email     = $('#q_email').val(),
            q_question  = $('#q_question').val();

        let urlData = `&q_name=${q_name}&q_email=${q_email}&q_question=${q_question}`;
        let contactForm = $("#contact-form-question");

        $.ajax({
            type: "POST",
            url: "http://jumper.digital/outkey/app/sendquestion.php",
            async: true,
            data: urlData,

            success: function() {
             console.log("Enviadno...")
            },

            beforeSend: function() {
                contactForm.find(".contact-form-question").remove()
                contactForm.append(`
                    <div class="sending-form">
                        <span class="loading-wrap">
                            <img src="http://serlares.com.br/wp-content/uploads/2016/11/loading.gif"/>
                        </span>
                    </div>
                `);
            },

            complete: function(){
                console.log("Enviado!");

                contactForm.find(".content-form").remove();
                contactForm.find(".sending-form").remove();
                contactForm.append(`
                    <div class="success-form">
                        <h1>Sua pergunta foi enviada!</h1>
                        <p>Agradecemos o contato, em breve retornaremos sua mensagem! =D</p>
                        <span class="fa fa-check-circle"></span>
                    </div>
                `);
            }

        });

        event.preventDefault();
    }

    componentDidMount() {
        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/14/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const answersRepeater = res.acf.answers_repeater.map(obj => obj);
            this.setState({ answersRepeater });

            const answersRepeaterCompany = res.acf.answers_repeater_company.map(obj => obj);
            this.setState({ answersRepeaterCompany });

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    render() {

        return (
            <section className="wrap-opening wrap-alt">

                <TitlePage pageTitle="Tire suas dúvidas" />

                <div className="content">

                    <h3 className="title-faq-tabs">
                        Esclareça suas dúvidas abaixo, selecionando qual 
                        categoria lhe atende.
                    </h3>

                    <Tabs onSelect={this.handleSelect} selectedIndex={2} >

                        <TabList>
                          <Tab>Para Usuários</Tab>
                          <Tab>Para Empresas</Tab>
                        </TabList>

                        <TabPanel>
                            <div className="wrap-answers answers-users">
                                <ul>
                                    {this.state.answersRepeater.map((post, index) =>
                                        <Accordion key={index} title={post.question}>
                                            {post.answer}
                                        </Accordion>
                                    )}
                                </ul>
                            </div>
                        </TabPanel>

                        <TabPanel>
                            <div className="wrap-answers answers-users">
                                <ul>
                                    {this.state.answersRepeaterCompany.map((post, index) =>
                                        <Accordion key={index} title={post.question_company}>
                                            {post.answer_company}
                                        </Accordion>
                                    )}
                                </ul>
                            </div>
                        </TabPanel>

                    </Tabs>

                </div>

                <div className="send-your-question">
                    <div className="content">
                        <div className="flex-question">
                            <div className="title-form">
                                <h2>Envie sua dúvida.</h2>
                            </div>

                            <form onSubmit={this.submitQuestion} id="contact-form-question">
                                <div className="contact-form-question">

                                    <div className="col-input">
                                        <input type="text" id="q_name" placeholder="Nome" className="question-input"/>
                                        <input type="text" id="q_email" placeholder="Email" className="question-input" />
                                    </div>
                                    <div className="col-input">
                                        <textarea id="q_question" placeholder="Sua pergunta" className="question-message" />
                                    </div>
                                    <div className="col-submit">
                                        <input className="submit-question" type="submit" value="Enviar dúvida"/>
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>

                <div className="anchor-contact">
                    <h1>
                        Entre em contato
                    </h1>
                    <p>Agende um atendimento.</p>
                    <Link onClick={this.toTop} to="/contato">Fale conosco</Link>
                </div>

            </section>
        )
    }

}

export default Answers;
