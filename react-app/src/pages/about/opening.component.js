import React, { Component } from 'react';
import { Link }             from 'react-router';
require('./about.style.scss');

class OpeningAbout extends Component{

    constructor(props) {
      super(props);

      this.state = {
        contents: [],
        team: [],
        clients: []
      };
    }

    toTop() {
        document.body.scrollTop = 0
    }

    componentDidMount() {

        let fetchUrl = 'http://www.jumper.digital/outkey/wp-json/wp/v2/pages/10/';

        fetch(fetchUrl).then(res => {
            return res.json();
        }).then(res => {

            const contents = res.acf.outkey_numbers.map(obj => obj);
            this.setState({ contents });

            const team = res.acf.outkey_team.map(obj => obj);
            this.setState({ team });

            const clients = res.acf.outkey_clients.map(obj => obj.clients_size);
            this.setState({ clients });
            console.log(clients);

        }).catch(function(err) {
            console.error('OOH SHIT', err);
        });
    }

    render() {

        let GIFload = 'http://www.jumper.digital/outkey/wp-content/uploads/2016/11/loading.gif';

        return (
            <div className="about-content">

                <section className="wrap-opening">
                    <div className="content">

                        <div className="flex-opening">
                            <div className="left-title wow animate-fade" data-wow-duration="0.5s" >
                                <h2>Como <br />Surgiu o<br /> OutKey</h2>
                            </div>
                            <div className="the-content-about">
                                <p>
                                    { this.props.acf ? this.props.acf['opening_text'] : 'Carregando ...' }
                                </p>
                            </div>
                            <div className="thumb-mobile">
                                <img src={ this.props.acf ? this.props.acf['about_opening_image'] : GIFload } alt="OutKey App"/>
                            </div>
                        </div>

                        <div className="outkey-numbers">
                            <ul>

                                {this.state.contents.map((post, index) =>
                                    <li key={index}>
                                        <h2>{post.number_outkey}</h2>
                                        <p>{post.number_description}</p>
                                    </li>
                                )}

                            </ul>
                        </div>
                    </div>
                </section>

                <section className="outkey-team">
                    <div className="section-title">
                        <h2>Equipe OutKey</h2>
                    </div>
                    <div className="list-team">
                        <ul>

                            {this.state.team.map((_team, index) =>
                                <li key={index} className="wow animate-fade" data-wow-duration="0.5s" data-wow-delay="0s">
                                    <div className="wrap-soldier" style={{backgroundImage: `url(${_team.picture_team})`}}>
                                        <div className="head-soldier">
                                            <h2>{_team.name_team}</h2>
                                            <p>{_team.work_team}</p>
                                        </div>
                                        <div className="social-media_soldier">
                                            <a href={_team.facebook}><span className="fa fa-facebook"></span></a>
                                            <a href={_team.instagram}><span className="fa fa-instagram"></span></a>
                                            <a href={_team.email}><span className="fa fa-envelope-o"></span></a>
                                        </div>
                                    </div>
                                </li>
                            )}

                        </ul>
                    </div>
                </section>

                <section className="client-team">
                    <div className="content">

                        <div className="flex-clients">

                            <div className="client-team_title">
                                <h1>
                                    Quem <b>conta com </b><b>a gente.</b>
                                </h1>
                            </div>

                            {this.state.clients.map((_clients, index) =>
                                <div className="brand-content" key={index}>
                                    <div className="brand-client">
                                        <img src={_clients.url} alt="OutKey App"/>
                                    </div>
                                </div>
                            )}

                        </div>

                        <div className="content-contact">
                            <h1>
                                Faça parte disso.
                            </h1>
                            <Link to="/contato" onClick={this.toTop}><span>entre em contato</span></Link>
                        </div>

                    </div>
                </section>

            </div>
        )
    }

}

export default OpeningAbout;
