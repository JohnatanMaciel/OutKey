<?php


    // **** CROP THEME
    // ***************
    add_theme_support('post-thumbnails');
    add_image_size( 'medio', 500, 500, true );


    // ================================================================================
    // CUSTOM ADMIN PAGE ==============================================================

    add_action('admin_head', 'my_custom_panel');

    function my_custom_panel() {
      echo '<style>
        #menu-comments {
            display: none !important;
        }
        .wp-menu-name {
            padding: 15px 0 !important;
            font-size: 0.9rem !important;
        }
        #adminmenu  {
            margin: 0 !important;
        }
        #adminmenu #menu-dashboard {
            background-color: rgb(91, 32, 156) !important;
        }
        #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu {
            background: #949494 !important;
        }
        #adminmenu li a {
            display: flex;
            align-items: center;
        }
        #adminmenu li.wp-menu-separator {
            margin: 10px 0 !important;
            background: #565656 !important;
            height: 1px !important;
        }
      </style>';
    }



    // *** //

    // ================================================================================
    // MENU PRINCIPAL =================================================================

    if (function_exists('register_nav_menus')) {
        register_nav_menus(
            array(
                'menu_principal' => 'Menu - Principal'
            )
        );
    }

    // MENU PRINCIPAL =================================================================
    // ================================================================================

    // *** //

