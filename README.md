## OutKey Site

O site se encontra na pasta /react-app

Para iniciar o desenvolvimento, vá até a pasta ./react-app e execute: `$ npm start`.

Para realizar o Deploy do Site, deveremos executar o comando `$npm run build`. Após executar o comando, o projeto compilado ficará dentro da pasta ./build.

Os arquivos encontrados na pasta `./build` devem ser colocados no diretório inicial do Site: `http://www.outkey.com.br`
